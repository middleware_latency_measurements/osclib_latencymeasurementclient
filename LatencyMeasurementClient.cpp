
#include "OSCLib/OSCLibrary.h"
#include "OSCLib/Data/OSCP/OSCPConstants.h"
#include "OSCLib/Data/OSCP/OSCPConsumer.h"
#include "OSCLib/Data/OSCP/OSCPConsumerEventHandler.h"
#include "OSCLib/Data/OSCP/OSCPConsumerNumericMetricStateHandler.h"
#include "OSCLib/Data/OSCP/OSCPProvider.h"
#include "OSCLib/Data/OSCP/OSCPProviderNumericMetricStateHandler.h"
#include "OSCLib/Data/OSCP/OSCPProviderComponentStateHandler.h"
#include "OSCLib/Data/OSCP/OSCPProviderHydraMDSStateHandler.h"
#include "OSCLib/Data/OSCP/MDIB/ChannelDescriptor.h"
#include "OSCLib/Data/OSCP/MDIB/CodedValue.h"
#include "OSCLib/Data/OSCP/MDIB/ComponentState.h"
#include "OSCLib/Data/OSCP/MDIB/HydraMDSDescriptor.h"
#include "OSCLib/Data/OSCP/MDIB/HydraMDSState.h"
#include "OSCLib/Data/OSCP/MDIB/MDDescription.h"
#include "OSCLib/Data/OSCP/MDIB/NumericMetricDescriptor.h"
#include "OSCLib/Data/OSCP/MDIB/NumericMetricState.h"
#include "OSCLib/Data/OSCP/MDIB/NumericMetricValue.h"
#include "OSCLib/Data/OSCP/MDIB/OperationInvocationContext.h"
#include "OSCLib/Data/OSCP/MDIB/StringMetricValue.h"
#include "OSCLib/Data/OSCP/MDIB/SystemContext.h"
#include "OSCLib/Data/OSCP/MDIB/LocalizedText.h"
#include "OSCLib/Data/OSCP/MDIB/VMDDescriptor.h"
#include "OSCLib/Data/OSCP/MDIB/RealTimeSampleArrayMetricState.h"
#include "OSCLib/Util/DebugOut.h"

#include "OSELib/OSCP/ServiceManager.h"

#include "Poco/Mutex.h"
#include "Poco/ScopedLock.h"
#include "Poco/Thread.h"
#include "Poco/Runnable.h"

#include <chrono>
#include "OSCLib/Util/LatencyMeasurement.h"

using namespace OSCLib;
using namespace OSCLib::Util;
using namespace OSCLib::Data::OSCP;

const std::string deviceEPR("UDI_12345");

const std::string MDS_HANDLE("mds_handle");
const std::string VMD_DESCRIPTOR_HANDLE("vmd_handle");
const std::string CHANNEL_DESCRIPTOR_HANDLE("channel_handle");


class ExampleConsumerEventHandler : public OSCPConsumerNumericMetricStateHandler {
public:
    ExampleConsumerEventHandler(const std::string & handle) :
    	currentWeight(0),
		handle(handle)
	{
    }

    void onStateChanged(const NumericMetricState & state) override {
        double val = state.getObservedValue().getValue();
        DebugOut(DebugOut::Default, "ExampleProject") << "Consumer: Received value changed of " << handle << ": " << val << std::endl;
        currentWeight = (float)val;
    }

    void onOperationInvoked(const OperationInvocationContext & oic, InvocationState is) override {
        DebugOut(DebugOut::Default, "ExampleProject") << "Consumer: Received operation invoked (ID, STATE) of " << handle << ": " << oic.transactionId << ", " << Data::OSCP::EnumToString::convert(is) << std::endl;
    }

    std::string getHandle() override {
        return handle;
    }

    float getCurrentWeight() {
        return currentWeight;
    }

private:
    float currentWeight;
    const std::string handle;
};

int main(int argc, char* argv[])
{
	if(argc != 4){
		std::cerr << "wrong number of arguments: " << argc << std::endl;
		std::cerr << "usage: ./LatencyMeasurementClient #numberOfMeasurements #sleepTimeBetweenMEasurements #sleepBeforeMeasurements" << std::endl;
		return 0;
	}

	LatencyMeasurement::getInstance().setNumOfMeasurements(atoi(argv[1]));
	LatencyMeasurement::getInstance().setDelayBetweenMeasurements(atoi(argv[2]));
	LatencyMeasurement::getInstance().setDelayBeforeMeasurements(atoi(argv[3]));


	auto now = std::chrono::high_resolution_clock::now();
	auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();

	std::cerr << "########: " << nanos << std::endl;

	DebugOut(DebugOut::Default, "ExampleProject") << "Startup";
    OSCLibrary::getInstance().startup();
	OSCLibrary::getInstance().setPortStart(11000);

	OSELib::OSCP::ServiceManager oscpsm;
    class MyHandler : public OSELib::OSCP::HelloReceivedHandler {
    public:
    	MyHandler() {
    	}
    	void helloReceived(const std::string & epr) override {
    		DebugOut(DebugOut::Default, "ExampleProject") << "Hello received! EPR: " << epr;
    	}
    };
    std::unique_ptr<MyHandler> myHandler(new MyHandler());
    oscpsm.setHelloReceivedHandler(myHandler.get());


	// Discovery
	std::shared_ptr<OSCPConsumer> c(oscpsm.discoverEndpointReference(deviceEPR));
    std::shared_ptr<ExampleConsumerEventHandler> eces1(new ExampleConsumerEventHandler("handle_metric"));


	if (c != nullptr) {
        OSCPConsumer & consumer = *c;
		DebugOut(DebugOut::Default, "ExampleProject") << "Discovery succeeded.";

		// MDIB test
		MDIBContainer mdib = consumer.getMDIB();

		// Register for metric event
        consumer.registerStateEventHandler(eces1.get());

		// Get state test (current value)
		NumericMetricState currentValueState;

	    int temp = -1;
	    while(temp != 0 && temp != 1){
			std::cout << "Insert \"1\" to start measurement " << std::endl
					<< "Insert \"0\" to stop application";
			std::cin >> temp;
	    }

	    if(temp == 0){
	    	oscpsm.setHelloReceivedHandler(nullptr);
	        OSCLibrary::getInstance().shutdown();
	        return 0;
	    }


	    //sleep before starting the measurements in order to be able to log off the
	    //ssh session
	    Poco::Thread::sleep(LatencyMeasurement::getInstance().getDelayBeforeMeasurements());


		LatencyMeasurement::getInstance().clearAllMeasurements();
		int sleepTime = LatencyMeasurement::getInstance().getDelayBetweenMeasurements();
		for(int measurementCounter = 0;
				measurementCounter < LatencyMeasurement::getInstance().getNumOfMeasurements();
				measurementCounter++)
		{
			
			Poco::Thread::sleep(sleepTime);
			
			auto nowT0 = std::chrono::steady_clock::now();
			long long nanosT0 = std::chrono::duration_cast<std::chrono::nanoseconds>(nowT0.time_since_epoch()).count();
//			std::cerr << "t0: " << nanosT0 << std::endl;
			LatencyMeasurement::getInstance().addElementToT0List(nanosT0);
			
			consumer.requestState("handle_metric", currentValueState);
			
			auto nowT6 = std::chrono::steady_clock::now();
			long long nanosT6 = std::chrono::duration_cast<std::chrono::nanoseconds>(nowT6.time_since_epoch()).count();
//			std::cerr << "t6: " << nanosT6 << std::endl;
			LatencyMeasurement::getInstance().addElementToT6List(nanosT6);
			
			
			double curValue = currentValueState.getObservedValue().getValue();
//			std::cerr << "Current Value: " << curValue << std::endl;
		}


		



        Poco::Thread::sleep(500);

        std::cout << "##############" << std::endl;

        LatencyMeasurement::getInstance().writeEveryMeasurementToFiles(true);

        consumer.unregisterStateEventHandler(eces1.get());
        Poco::Thread::sleep(500);

		consumer.disconnect();
	} else {
		DebugOut(DebugOut::Default, "ExampleProject") << "Discovery failed.";
	}

	oscpsm.setHelloReceivedHandler(nullptr);


    OSCLibrary::getInstance().shutdown();
	DebugOut(DebugOut::Default, "ExampleProject") << "Shutdown." << std::endl;
}
