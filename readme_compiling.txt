compiling and linking with command line:
========================================
Attention: customize the paths for includes

g++ -std=c++11 -D_LINUX -I/home/martin/workspace_OSCLib/osclib/include -I/home/martin/workspace_OSCLib/osclib/datamodel -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"LatencyMeasurementClient.d" -MT"LatencyMeasurementClient.o" -o "LatencyMeasurementClient.o" "LatencyMeasurementClient.cpp"

g++ -L/home/martin/workspace_OSCLib/osclib -o "LatencyMeasurementClient"  ./LatencyMeasurementClient.o   -lOSCLib -lpthread -lxerces-c
